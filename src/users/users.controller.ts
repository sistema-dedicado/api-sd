import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersService } from './users.service';
import { Body, ClassSerializerInterceptor, Controller, Delete, Get, Param, Post, Put, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guard/ jwt-auth.guard';

@ApiTags("user")
@Controller("users")
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async findMany() {
    return await this.userService.findMany();
  }

  @UseGuards(JwtAuthGuard)
  @ApiSecurity("access-key")
  @UseInterceptors(ClassSerializerInterceptor)
  @Get(":id")
  async findUnique(@Param("id") id: string) {
    return await this.userService.findUnique(String(id));
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return await this.userService.create(createUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiSecurity("access-key")
  @UseInterceptors(ClassSerializerInterceptor)
  @Put(":id")
  async update(@Param("id") id: string, @Body() updateUserDto: UpdateUserDto) {
    return await this.userService.update(String(id), updateUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiSecurity("access-key")
  @UseInterceptors(ClassSerializerInterceptor)
  @Delete(":id")
  async delete(@Param("id") id: string) {
    return await this.userService.delete(String(id));
  }
}
