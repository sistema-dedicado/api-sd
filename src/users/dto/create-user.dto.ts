import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsEmpty, IsNotEmpty, IsString, Length, Matches } from "class-validator";
import { RegExHelper } from "src/helpers/regex.helper";

export class CreateUserDto {
  @ApiPropertyOptional()
  @IsString()
  @Length(5, 150)
  readonly name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @Length(5, 50)
  readonly email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Length(11, 14)
  readonly phone: string;

  @ApiPropertyOptional()
  @IsEmpty()
  readonly image: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Length(8, 25)
  @Matches(RegExHelper.password)
  readonly password: string;
}