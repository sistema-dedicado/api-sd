export class UserEntity {
  id?: string;
  name?: string;
  email: string;
  phone?: string;
  image?: string;
  password: string;
}
