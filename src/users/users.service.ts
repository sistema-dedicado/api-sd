import { LoginAuthDto } from './../auth/dto/login-auth.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Injectable, NotFoundException, HttpException, HttpStatus } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma/prisma.service';
import { User, Prisma } from "@prisma/client";

import { compareSync, hash } from "bcrypt";

interface AuthLogin extends Partial<User> {
  email: string;
}

@Injectable()
export class UsersService {
  constructor(private readonly prismaService: PrismaService) {}

  async findMany(): Promise<User[]> {
    const users = await this.prismaService.user.findMany({
      include: {
        rules: true,
        accounts: true,
        sessions: true,
      },
    });
    return users;
  }

  async findUnique(id: string): Promise<User> {
    const user = await this.prismaService.user.findUnique({
      where: { id },
      include: {
        rules: true,
        accounts: true,
        sessions: true,
      },
    });
    try {
      if (!user) {
        throw new NotFoundException(`User ${id} not found`);
      }
      return await user;
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError && error.code) {
        throw new NotFoundException(`User ${id} not found`);
      }
      throw error;
    }
  }

  async findByLogin({ email, password }: LoginAuthDto): Promise<AuthLogin> {
    const user = await this.prismaService.user.findFirst({
      where: { email },
    });
    if (!user) {
      throw new HttpException("INVALID CREDENTIALS", HttpStatus.UNAUTHORIZED);
    }

    const passwordVerify = await compareSync(password, user.password);
    if (!passwordVerify) {
      throw new HttpException("INVALID CREDENTIALS", HttpStatus.UNAUTHORIZED);
    }

    const { password: p, ...rest } = user;
    return rest;
  }

  async findByPayload({ email }: any): Promise<any> {
    return await this.prismaService.user.findFirst({
      where: { email },
    });
  }

  //

  async create(createUserDto: CreateUserDto): Promise<User> {
    const verifyEmail = await this.prismaService.user.findFirst({
      where: { email: createUserDto.email },
    });
    const verifyPhone = await this.prismaService.user.findFirst({
      where: { phone: createUserDto.phone },
    });
    if (verifyEmail) {
      throw new HttpException("This E-mail already exist!", HttpStatus.CONFLICT);
    }
    if (verifyPhone) {
      throw new HttpException("This Phone already exist!", HttpStatus.CONFLICT);
    }

    const tokenRandom = Math.random().toString(16).substr(2, 8).toUpperCase();
    const data: Prisma.UserCreateInput = {
      ...createUserDto,
      password: await hash(createUserDto.password, 10),
      token: tokenRandom,
    };
    const createUser = await this.prismaService.user.create({ data });
    return {
      ...createUser,
      password: undefined,
      token: undefined,
    };
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const tokenRandom = Math.random().toString(16).substr(2, 8).toUpperCase();
    const user = await this.prismaService.user.findUnique({ where: { id } });
    try {
      if (!user) {
        throw new NotFoundException(`User ${id} not found`);
      }
      const data: Prisma.UserUpdateInput = {
        ...updateUserDto,
        password: await hash(updateUserDto.password, 10),
        token: tokenRandom,
      };
      return await this.prismaService.user.update({
        data,
        where: { id },
      });
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError && error.code) {
        throw new NotFoundException(`User ${id} not found`);
      }
      throw error;
    }
  }

  async delete(id: string): Promise<User> {
    const user = await this.prismaService.user.findUnique({ where: { id } });
    try {
      if (!user) {
        throw new NotFoundException(`User ${id} not found`);
      }
      return await this.prismaService.user.delete({
        where: { id },
      });
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError && error.code) {
        throw new NotFoundException(`User ${id} not found`);
      }
      throw error;
    }
  }
}
