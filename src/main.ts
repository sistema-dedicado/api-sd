import { PrismaService } from "./prisma/prisma.service";
import { NestFactory, Reflector } from "@nestjs/core";
import { AppModule } from "./app.module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { ClassSerializerInterceptor, ValidationPipe } from "@nestjs/common";
import { ExpressAdapter, NestExpressApplication } from "@nestjs/platform-express";
import * as express from "express";
import * as functions from "firebase-functions";

const server: express.Express = express();
export const createNestServer = async (expressInstance: express.Express) => {
  const adapter = new ExpressAdapter(expressInstance);
  const app = await NestFactory.create<NestExpressApplication>(AppModule, adapter, {});

  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app);

  const config = new DocumentBuilder()
    .setTitle("Sistema Dedicado")
    .setDescription("API dos Sistemas Integrados ERP e CRM da Sistema Dedicado")
    .setVersion("1.0")
    .setContact("Suporte Dedicado", "https://suporte.dedicado.digital", "master@dedicado.digital")
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("", app, document, {
    customSiteTitle: "API Sistema Dedicado",
  });

  app.useGlobalPipes(new ValidationPipe());

  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  return app.init();
};
createNestServer(server)
  .then(v => console.log("API SD Ready"))
  .catch(err => console.error("API SD broken", err));
export const apiMain: functions.HttpsFunction = functions.https.onRequest(server);
