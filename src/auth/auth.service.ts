import { LoginAuthDto } from './dto/login-auth.dto';
import { JwtPayload } from './interfaces/jwt-payload';
import { PasswordAuthDto } from './dto/password-auth.dto';
import { User } from '@prisma/client';
import { UsersService } from './../users/users.service';
import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compareSync, hash } from 'bcrypt';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class AuthService {
  constructor(private readonly prismaService: PrismaService, private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

  private _createToken({ email }): any {
    const user: JwtPayload = { email };
    const Authorization = this.jwtService.sign(user);
    return {
      expiresIn: process.env.JWT_EXPIRES_IN,
      Authorization,
    };
  }

  async login(loginAuthDto: LoginAuthDto): Promise<any> {
    const user = await this.usersService.findByLogin(loginAuthDto);
    const token = this._createToken(user);
    return {
      ...token,
      data: user,
    };
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    const user = await this.usersService.findByPayload(payload);
    if (!user) {
      throw new HttpException("INVALID TOKEN", HttpStatus.UNAUTHORIZED);
    }
    return user;
  }

  async updatePassword(payload: PasswordAuthDto, id: string): Promise<User> {
    const user = await this.prismaService.user.findUnique({
      where: { id },
    });
    if (!user) {
      throw new HttpException("INVALID CREDENTIALS", HttpStatus.UNAUTHORIZED);
    }
    // compare passwords
    const areEqual = await compareSync(payload.old_password, user.password);
    if (!areEqual) {
      throw new HttpException("INVALID CREDENTIALS", HttpStatus.UNAUTHORIZED);
    }
    return await this.prismaService.user.update({
      where: { id },
      data: { password: await hash(payload.new_password, 10) },
    });
  }
}
