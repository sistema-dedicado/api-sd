import { LoginAuthDto } from './dto/login-auth.dto';
import { AuthService } from './auth.service';
import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  public async login(@Body() loginAuthDto: LoginAuthDto): Promise<any> {
    return await this.authService.login(loginAuthDto);
  }
}
